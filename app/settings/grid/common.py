# settings
DEFAULT_EVENT_IMAGE_PATH = 'app/static/5p logo.jpg'
FONT_PATH = 'app/static/fonts/Gidole-Regular.ttf'

ICONS_PATHS = [
    'app/static/icons/location icon.jpg',
    'app/static/icons/crew icon.jpg'
]

PERSON_ICON_PATHS = {
    'female': 'app/static/icons/girl icon.jpg',
    'male': 'app/static/icons/boy icon.jpg',
}
