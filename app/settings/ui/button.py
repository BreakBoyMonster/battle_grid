# common
BUTTON_TEXT_COLOR = 'red'

DESTINATION_BUTTON_TITLE = 'choose folder'
SAVE_EVENT_NAME_BUTTON_TITLE = 'save'
CREATE_NEW_EVENT_BUTTON_TITLE = 'create new'
RENAME_EVENT_BUTTON_TEXT = 'rename'
REGISTER_BUTTON_TITLE = 'register'
UNREGISTER_BUTTON_TITLE = 'unregister'
OPEN_ADD_CATEGORY_TOPLEVEL_BUTTON_TEXT = 'add category'
REMOVE_CATEGORY_BUTTON_TEXT = 'remove category'

DESTINATION_BUTTON_SIZE = '12'
DESTINATION_BUTTON_COORDS = {'row': 5, 'column': 1, 'sticky': 'W', 'padx': (5, 0)}

EVENT_BUTTON_FRAME_COORDS = {
    'row': 2, 'column': 0, 'columnspan': 2, 'pady': (0, 10)
}

SAVE_EVENT_NAME_BUTTON_SIZE = '7'
SAVE_EVENT_NAME_BUTTON_COORDS = {'row': 0, 'column': 0, 'sticky': 'W'}

CREATE_NEW_EVENT_BUTTON_SIZE = '9'
CREATE_NEW_EVENT_NAME_BUTTON_COORDS = {'row': 0, 'column': 1, 'sticky': 'W'}

RENAME_EVENT_BUTTON_SIZE = '7'
RENAME_EVENT_BUTTON_COORDS = {'row': 0, 'column': 2, 'sticky': 'W'}

OPEN_ADD_CATEGORY_TOPLEVEL_BUTTON_SIZE = '10'
OPEN_ADD_CATEGORY_TOPLEVEL_BUTTON_COORDS = {
    'row': 9, 'column': 2, 'sticky': 'W'
}
REMOVE_CATEGORY_BUTTON_SIZE = '12'
REMOVE_CATEGORY_BUTTON_COORDS = {'row': 9, 'column': 3, 'sticky': 'W'}

# tab control

REGISTER_BUTTON_SIZE = '7'
UNREGISTER_BUTTON_SIZE = '8'
REGISTER_BUTTON_COORDS = {'row': 6, 'column': 1, 'sticky': 'W'}
UNREGISTER_BUTTON_COORDS = {
    'row': 7, 'column': 1, 'pady': (5, 0), 'sticky': 'W'
}

OPEN_EDIT_CATEGORY_TOPLEVEL_BUTTON_TITLE = 'edit'
OPEN_EDIT_CATEGORY_TOPLEVEL_BUTTON_SIZE = '6'
OPEN_EDIT_CATEGORY_TOPLEVEL_BUTTON_COORDS = {
    'row': 4, 'column': 0, 'sticky': 'W', 'padx': (6, 0)
}

GENERATE_CATEGORY_GRID_BUTTON_TITLE = 'generate grid'
GENERATE_CATEGORY_GRID_BUTTON_SIZE = '7'
GENERATE_CATEGORY_GRID_BUTTON_COORDS = {
    'row': 5, 'column': 0, 'sticky': 'W', 'padx': (6, 0)
}

# additional windows

# change event name
SAVE_NEW_EVENT_TITLE_BUTTON_TITLE = 'save'
SAVE_NEW_EVENT_TITLE_BUTTON_SIZE = '7'

SAVE_NEW_EVENT_TITLE_BUTTON_COORDS = {'row': 2, 'column': 0, 'pady': (0, 5)}

# save category
SAVE_CATEGORIES_BUTTON_TEXT = 'save'
SAVE_CATEGORIES_BUTTON_SIZE = '7'
SAVE_CATEGORIES_BUTTON_COORDS = {
    'row': 5, 'column': 0, 'pady': (0, 5), 'columnspan': 20
}

# create category
ADD_CATEGORY_BUTTON_COORDS = {
    'row': 5, 'column': 0, 'pady': (0, 5), 'columnspan': 20
}
ADD_CATEGORY_BUTTON_TEXT = 'create'
ADD_CATEGORY_BUTTON_SIZE = '7'
