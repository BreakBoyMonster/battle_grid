__all__ = (
    'CATEGORY_TOPLEVEL_FIELDS',
)

CATEGORY_TOPLEVEL_FIELDS = [
    '_add_category_toplevel',
    '_category_input'
]
